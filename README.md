# PlugNG - plugin system, done for Dart

> **DISCLAIMER**: this library is neither production-ready nor even publication-ready. 
> It's developed as a byproduct of development of my other project and I didn't use it yet ever there.
> Literally anything can go wrong with it, and any public API existent can dramatically change.
> It may get an 1.0.0 release someday (and things will go different), it may not. I don't know. 
> 
> Do not rely on it. I promise you, it will be nothing but pain.