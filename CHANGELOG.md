## 0.1.2-alpha

- Fixed: `VersionCondition` didn't work with whitespaces.

## 0.1.1-alpha

- Initial version, not heavily tested.
  - Developed as a byproduct, use at own risk!
