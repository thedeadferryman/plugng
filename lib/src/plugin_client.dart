/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import 'dart:async';
import 'dart:isolate';
import 'dart:mirrors';

import 'intercom.dart';
import 'messaging.dart';
import 'plugin_registry.dart';
import 'plugng_base.dart';
import 'util.dart';

class DependencyInstance {
  final PluginIntercom _intercom;
  final String uid;

  DependencyInstance.__(this._intercom, this.uid);

  Future<CallResult> call(
      String method, //
      dynamic args,
      {Duration? timeout}) async {
    var call = PluginCall.unique(uid, method);
    var msg = Message(MessageType.PLUGIN_CALL, call);

    var rsp = await _intercom.sendWaitWhere(
        msg,
        Filters.every([
          Filters.ofType([
            MessageType.CALL_RESPONSE,
            MessageType.CALL_NO_PLUGIN,
          ]),
          _checkResultId(call.id)
        ]));

    if (rsp.type == MessageType.CALL_NO_PLUGIN) throw PluginNotLoadedError(uid);

    return CallResult.fromJson(assertTyped(rsp.content));
  }

  @override
  Future<CallResult> noSuchMethod(Invocation invocation) {
    if (!invocation.isMethod) {
      throw NoSuchMethodError.withInvocation(this, invocation);
    }

    dynamic args;

    if (invocation.positionalArguments.length == 1 &&
        invocation.namedArguments.isEmpty) {
      args = invocation.positionalArguments[0];
    } else if (invocation.positionalArguments.isEmpty &&
        invocation.namedArguments.isNotEmpty) {
      args = invocation.namedArguments
          .map((key, value) => MapEntry(MirrorSystem.getName(key), value));
    } else if (invocation.positionalArguments.isNotEmpty &&
        invocation.namedArguments.isEmpty) {
      args = invocation.positionalArguments;
    } else if (invocation.positionalArguments.isEmpty &&
        invocation.namedArguments.isEmpty) {
      args = null;
    } else {
      throw NoSuchMethodError.withInvocation(this, invocation);
    }

    return call(MirrorSystem.getName(invocation.memberName), args);
  }

  Filter _checkResultId(String id) =>
      (Message result) => (result.type == MessageType.CALL_RESPONSE)
          ? CallResult.fromJson(assertTyped(result.content)).id == id
          : result.content == id;
}

void _noop() => null;

class PluginInstance {
  final PluginIntercom intercom;
  final Plugin self;

  PluginInstance(
    this.self,
    ReceivePort inPort,
    SendPort outPort,
  ) : intercom = PluginIntercom(inPort, outPort);

  Future<DependencyInstance> getDependency(PluginRequest request) async {
    var msg = Message(MessageType.DEPENDENCY_REQUEST, request);
    var rsp = await intercom.sendWaitWhere(
      msg,
      Filters.every([
        Filters.ofType([
          MessageType.DEPENDENCY,
          MessageType.DEPENDENCY_MISSING,
        ]),
        _checkDepName(request.depName),
      ]),
    );

    if (rsp.type == MessageType.DEPENDENCY_MISSING) {
      throw PluginNotFoundError(request);
    }

    var dep = Dependency.fromJson(assertTyped(rsp.content));

    return DependencyInstance.__(intercom, dep.uid);
  }

  int bindMethod<R, T>(String name, FutureOr<R> Function(T args) body) {
    if (intercom.isInitialized) {
      throw StateError('Cannot bind to running instance');
    }

    return intercom.listener.addListener((data) {
      try {
        var msg = Message.fromJson(assertTyped(data));

        if (msg.type != MessageType.CALL_REQUEST) {
          return true;
        }

        var req = CallRequest.fromJson((assertTyped(msg.content)));

        if (req.method != name) return true;

        _handleMethodCall(name, body, req);

        return false;
      } on TypeAssertionError {
        return true;
      }
    });
  }

  void _handleMethodCall<T, R>(
      String name, FutureOr<R> Function(T args) body, CallRequest req) async {
    try {
      var res = await body(assertTyped(req.args));

      intercom.sendSignal(Message(MessageType.PLUGIN_CALL_RESULT, res));
    } on TypeAssertionError catch (e) {
      intercom.sendSignal(Message(
        MessageType.PLUGIN_CALL_RESULT,
        CallResult(req.id, 'ArgumentError: ${e.what}', error: true),
      ));
    }
  }

  void start([void Function() onStarted = _noop]) {
    intercom.listener.prependListener((data) {
      try {
        var msg = Message.fromJson(assertTyped(data));

        if (msg.type == MessageType.INTRO_REQUEST) {
          intercom.sendSignal(Message(MessageType.PLUGIN_INTRO, self));
          return false;
        }

        if (msg.type == MessageType.START_SIGNAL) {
          onStarted();
          return false;
        }

        return true;
      } on TypeAssertionError {
        return false;
      }
    });

    intercom.listener.addPassthroughListener((data) {
      try {
        var msg = Message.fromJson(assertTyped(data));

        if (msg.type == MessageType.CALL_REQUEST) {
          var call = CallRequest.fromJson(assertTyped(msg.content));

          intercom.sendSignal(Message(MessageType.PLUGIN_CALL_RESULT,
              "No such method '${call.method}'\n\tfor call id:${call.id}"));
        }
      } on TypeAssertionError {
        return;
      }
    });

    intercom.initialize();
  }

  Filter _checkDepName(String name) =>
      (Message msg) => (msg.type == MessageType.DEPENDENCY)
          ? Dependency.fromJson(assertTyped(msg.content)).name == name
          : msg.content == name;
}
