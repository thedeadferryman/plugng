/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import 'util.dart';

enum MessageType {
  INTRO_REQUEST, // Messages related to plugin identification
  PLUGIN_INTRO,
  DEPENDENCY_REQUEST, // Dependency tracking request
  DEPENDENCY,
  DEPENDENCY_MISSING,
  STOP_SIGNAL,
  START_SIGNAL,
  PLUGIN_DONE,
  PLUGIN_CALL,
  PLUGIN_CALL_RESULT,
  CALL_REQUEST,
  CALL_RESPONSE,
  CALL_NO_PLUGIN,
}

class Message extends Serializable {
  final MessageType type;

  final dynamic content;

  factory Message.fromJson(Map map) {
    var proxy = TypedMapProxy(map);

    return Message(
      MessageType.values[proxy.getTyped<int>('type')],
      proxy['content'],
    );
  }

  Message(this.type, [this.content]);

  @override
  Map<String, dynamic> toJson() => {
        'type': type.index,
        'content': (content is Serializable) ? content.toJson() : content,
      };

  @override
  String toString() => {
        'type': type.toString(),
        'content': (content is Serializable) ? content.toJson() : content,
      }.toString();
}
