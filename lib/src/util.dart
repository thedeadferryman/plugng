/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import 'package:collection/collection.dart';
import 'package:tuple/tuple.dart';

class TypeAssertionError extends Error {
  final Type expected;
  final Type actual;

  TypeAssertionError(this.expected, this.actual);

  String get what => 'Expected $expected, got $actual';
}

T assertTyped<T>(dynamic val, {String name = 'value'}) {
  if (!(val is T)) {
    throw TypeAssertionError(T, val.runtimeType);
  }

  return val;
}

class TypedMapProxy extends DelegatingMap {
  TypedMapProxy(Map base) : super(base);

  T getTyped<T>(String key) => assertTyped(this[key], name: key);
}

class OrderedListener<T> {
  List<Tuple2<int, bool Function(T)>> _listeners = [];
  int _nextId = 0;

  void onEvent(T data) {
    for (var listener in _listeners) {
      var res = listener.item2(data);

      if (!res) break;
    }
  }

  int addListener(bool Function(T) onEvent) {
    var lt = _registerListener(onEvent);

    _listeners.add(lt);

    return lt.item1;
  }

  int addPassthroughListener(void Function(T) onEvent) => addListener(
        (data) {
          onEvent(data);
          return true;
        },
      );

  int prependListener(bool Function(T) onEvent) {
    var lt = _registerListener(onEvent);

    _listeners = [
      [lt],
      _listeners
    ].expand((e) => e).toList();

    return lt.item1;
  }

  void removeListener(int id) => _listeners.removeWhere((e) => e.item1 == id);

  Tuple2<int, bool Function(T)> _registerListener(bool Function(T) listener) {
    var id = _nextId++;

    return Tuple2(id, listener);
  }
}

abstract class Serializable {
  const Serializable();

  dynamic toJson();

  @override
  String toString() => toJson().toString();
}
