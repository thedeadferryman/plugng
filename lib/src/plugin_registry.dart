/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import 'dart:convert';
import 'dart:io';

import 'package:collection/collection.dart';
import 'package:path/path.dart' as p;
import 'package:plugng/plugng_manager.dart';
import 'package:tuple/tuple.dart';
import 'package:version/version.dart';

import 'intercom.dart';
import 'messaging.dart';
import 'util.dart';
import 'version_comparator.dart';

class Plugin extends Serializable {
  final String name;
  final Version version;

  factory Plugin.fromJson(Map map) {
    var proxy = TypedMapProxy(map);

    return Plugin(
      proxy.getTyped('name'),
      Version.parse(proxy.getTyped('version')),
    );
  }

  Plugin(this.name, this.version);

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'version': version.toString(),
      };
}

class KnownPlugin extends Plugin {
  final Uri location;

  KnownPlugin(String name, Version version, this.location)
      : super(name, version);

  KnownPlugin.forPlugin(Plugin plugin, Uri location)
      : this(plugin.name, plugin.version, location);

  factory KnownPlugin.fromJson(Map map) {
    var proxy = TypedMapProxy(map);

    return KnownPlugin(
      proxy.getTyped('name'),
      Version.parse(proxy.getTyped('version')),
      Uri.parse(proxy.getTyped('location')),
    );
  }

  @override
  Map<String, dynamic> toJson() => {
        ...super.toJson(),
        'location': location.toString(),
      };
}

class PluginRequest extends Serializable {
  final String pluginName;
  final String depName;
  final VersionCondition version;

  PluginRequest(this.pluginName,
      {this.version = VersionCondition.any, String? name})
      : depName = name ?? pluginName;

  static PluginRequest fromJson(Map map) {
    var proxy = TypedMapProxy(map);

    return PluginRequest(proxy.getTyped('name'),
        version: VersionCondition.parse(proxy.getTyped('version')),
        name: proxy.getTyped('name'));
  }

  @override
  Map<String, dynamic> toJson() => {
        'name': pluginName,
        'depName': depName,
        'version': version.toString(),
      };

  bool check(Plugin plugin) =>
      plugin.name == pluginName && version.satisfies(plugin.version);

  @override
  String toString() => '$pluginName+($version)';
}

class Dependency extends Serializable {
  final String name;
  final String uid;

  Dependency(this.name, this.uid);

  static Dependency fromJson(Map map) {
    var proxy = TypedMapProxy(map);

    return Dependency(
      proxy.getTyped('name'),
      proxy.getTyped('uid'),
    );
  }

  @override
  Map toJson() => {
        'name': name,
        'uid': uid,
      };
}

class PluginRegistry {
  final List<KnownPlugin> _registry;

  PluginRegistry.identity() : _registry = [];

  PluginRegistry.__(Iterable<KnownPlugin> req)
      : _registry = req.toList()
          ..sort((a, b) => b.version.compareTo(a.version));

  static Stream<FileSystemEntity> _findAllPluginFiles(
    String lookupPath,
    List<String> lookupExts,
  ) =>
      Directory(lookupPath) //
          .list(recursive: true, followLinks: true)
          .where(
            (e) => lookupExts.contains(p.extension(e.path)),
          );

  static Future<PluginRegistry> scan({
    required String lookupPath,
    List<String> lookupExts = const ['.dart', '.dill'],
  }) async {
    var res = await _findAllPluginFiles(p.absolute(lookupPath), lookupExts)
        .map(
          (e) async {
            var iip = ManagerIntercom(e.uri);
            await iip.initialize(timeout: Duration(seconds: 2));
            return iip;
          },
        )
        .asyncMap((e) => e.then(
              (e) async {
                var res = Tuple2(
                    await e.sendWaitWhere(
                      Message(MessageType.INTRO_REQUEST),
                      Filters.ofType([MessageType.PLUGIN_INTRO]),
                    ),
                    e.uri);
                e.close();
                return res;
              },
            ))
        .map((e) => KnownPlugin.forPlugin(
              Plugin.fromJson(assertTyped(e.item1.content)),
              e.item2,
            ))
        .toList();

    return PluginRegistry.__(res);
  }

  static Future<PluginRegistry> fromFile(File file) async {
    var data = json.decode(await file.readAsString());

    return PluginRegistry.__(
      assertTyped<Iterable>(data)
          .map((e) => KnownPlugin.fromJson(assertTyped(e)))
          .toList(),
    );
  }

  KnownPlugin? findPlugin(PluginRequest request) =>
      _registry.firstWhereOrNull((element) => request.check(element));

  Future<File> dumpToFile(File file) =>
      file.writeAsString(jsonEncode(_registry));
}
