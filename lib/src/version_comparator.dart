/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import 'package:version/version.dart';

import 'util.dart';

enum Conditional { LESS, LESS_EQUAL, EQUAL, GREATER_EQUAL, GREATER }

class _SingleCondition {
  static const COND_SIGNS = {
    '<': Conditional.LESS,
    '<=': Conditional.LESS_EQUAL,
    '==': Conditional.EQUAL,
    '>=': Conditional.GREATER_EQUAL,
    '>': Conditional.GREATER,
  };

  final Version version;
  final Conditional conditional;

  static _SingleCondition parse(String e) {
    var entries = COND_SIGNS.entries.toList()
      ..sort((a, b) => b.key.length.compareTo(a.key.length));

    for (var entry in entries) {
      if (entry.key.matchAsPrefix(e) != null) {
        return _SingleCondition(
          Version.parse(e.replaceFirst(entry.key, '')),
          entry.value,
        );
      }
    }

    return _SingleCondition(Version.parse(e), Conditional.EQUAL);
  }

  static String signByType(Conditional cond) =>
      COND_SIGNS.entries.firstWhere((e) => e.value == cond).key;

  _SingleCondition(this.version, this.conditional);

  bool satisfies(Version ver) {
    switch (conditional) {
      case Conditional.LESS:
        return ver < version;
      case Conditional.LESS_EQUAL:
        return ver <= version;
      case Conditional.EQUAL:
        return ver == version;
      case Conditional.GREATER_EQUAL:
        return ver >= version;
      case Conditional.GREATER:
        return ver > version;
    }
  }

  @override
  String toString() => '${signByType(conditional)}$version';
}

class VersionCondition extends Serializable {
  static const SEPARATOR = ',';

  static const any = _AnyVersionCondition();

  final List<_SingleCondition> _conditions;

  const VersionCondition.__(this._conditions);

  factory VersionCondition.parse(String s) {
    if (s.trim() == '*') return any;

    return VersionCondition._parse(s);
  }

  VersionCondition._parse(String s)
      : this.__(s
            .split(SEPARATOR) //
            .map((e) => _SingleCondition.parse(e.trim()))
            .toList());

  bool satisfies(Version version) =>
      _conditions.every((cond) => cond.satisfies(version));

  @override
  String toString() => _conditions.map((e) => e.toString()).join(SEPARATOR);

  @override
  String toJson() => toString();
}

class _AnyVersionCondition extends VersionCondition {
  const _AnyVersionCondition() : super.__(const []);

  @override
  bool satisfies(Version version) => true;

  @override
  String toString() => '*';
}
