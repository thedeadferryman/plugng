/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import 'dart:async';
import 'dart:isolate';

import 'messaging.dart';
import 'util.dart';

/// Represents a message filter for [IntercomBase.sendWaitWhere] method.
typedef Filter = bool Function(Message message);

abstract class IntercomBase {
  ReceivePort get _inPort;

  SendPort get _outPort;

  /// Composite listener bound to plugin's output port.
  OrderedListener<dynamic> get listener;

  /// Shows whether intercom was initialized.
  bool get isInitialized;

  FutureOr<void> initialize();

  void _onEvent(dynamic data) => listener.onEvent(data);

  /// Sends a message to another side of channel and waits for the response.
  ///
  /// Recognition of the response message is performed by the given [filter] function.
  /// See [Filters] for some useful filter implementations.
  ///
  /// If [timeout] is given and there's no suitable response message within the timeout,
  /// method fails with [TimeoutException].
  Future<Message> sendWaitWhere(Message message, Filter filter,
      {Duration? timeout}) {
    var comp = Completer<Message>();

    var lsid = listener.prependListener((e) {
      try {
        var msg = Message.fromJson(assertTyped(e));
        if (filter(msg)) {
          comp.complete(msg);
          return false;
        }
      } on TypeAssertionError {
        return true;
      }

      return true;
    });

    var future = comp.future.then((value) {
      listener.removeListener(lsid);
      return value;
    });

    sendSignal(message);

    return (timeout != null) ? future.timeout(timeout) : future;
  }

  /// Sends a message to another side of channel.
  ///
  /// This method does not care if message was received or somehow processed.
  /// Think about it as of UDP packet.
  void sendSignal(Message message) => sendRaw(message.toJson());

  /// Sends a raw data chunk to another side of channel.
  ///
  /// This method is identical to [sendSignal], except the fact that it
  /// does not do any preprocessing on data supplied and does not ensure
  /// that it can be sent through the channel.
  ///
  /// More about limitations of the channel you can read at [SendPort.send].
  void sendRaw(data) => _outPort.send(data);

  /// Closes the input port of the channel, allowing to destroy the intercom correctly.
  void close() {
    _inPort.close();
  }
}

/// Represents a two-sided communication channel between a plugin and a manager.
///
/// [ManagerIntercom] is meant to handle a communication with a plugin for a plugin manager.
/// Moreover, it handles plugin init process (refer to [initialize] for more details).
class ManagerIntercom extends IntercomBase {
  /// Path to the plugin executable.
  late final Uri uri;

  /// Running plugin instance
  late final Isolate isolate;
  @override
  late final ReceivePort _inPort;
  @override
  late final SendPort _outPort;

  @override
  final OrderedListener<dynamic> listener = OrderedListener();
  bool _initialized = false;

  /// Creates an instance of [ManagerIntercom], does not initialize anything.
  ManagerIntercom(this.uri);

  @override
  bool get isInitialized => _initialized;

  /// Initializes the intercom.
  ///
  /// This is the keypoint of all plugin system, where [Isolate] for plugin is spawned.
  /// There's an option to pass [args] (refer to [Isolate.spawnUri]).
  ///
  /// After isolate has been spawned, it waits for plugin to send its [SendPort] to support
  /// two-sided communication. If not-null [timeout] was given, initialization fails
  /// with [TimeoutException] if plugin didn't send its [SendPort] before the timeout.
  @override
  Future<void> initialize(
      {List<String> args = const [],
      Duration timeout = const Duration(seconds: 2)}) async {
    var comp = Completer<void>();

    _inPort = ReceivePort();

    isolate = await Isolate.spawnUri(uri, args, _inPort.sendPort);

    var lsid = listener.prependListener((message) {
      if (!_initialized) {
        if (message is SendPort) {
          _initialized = true;
          _outPort = message;

          comp.complete();
          return false;
        }
      }
      return true;
    });

    _inPort.listen(_onEvent);

    return comp.future.timeout(timeout).then((value) {
      listener.removeListener(lsid);
      return value;
    }).onError((error, stackTrace) {
      listener.removeListener(lsid);
      _inPort.close();
      if (error != null) throw error;
    });
  }
}

/// Represents a two-sided communication channel between a plugin and a manager.
///
/// [PluginIntercom] is meant to handle a communication with a plugin manager for a plugin.
class PluginIntercom extends IntercomBase {
  @override
  final ReceivePort _inPort;
  @override
  final SendPort _outPort;
  @override
  final OrderedListener listener = OrderedListener();
  bool _initialized = false;

  /// Creates an instance of [PluginIntercom], does not initialize anything.
  PluginIntercom(this._inPort, this._outPort);

  @override
  bool get isInitialized => _initialized;

  /// Binds a listener to given [ReceivePort] and sends its corresponding [SendPort]
  /// to the plugin manager.
  @override
  void initialize() {
    _initialized = true;
    _inPort.listen(_onEvent);
    _outPort.send(_inPort.sendPort);
  }
}

/// A collection of useful message filters for [IntercomBase.sendWaitWhere].
abstract class Filters {
  /// Checks if message type is in given [types] list.
  static Filter ofType(List<MessageType> types) =>
      (Message msg) => types.contains(msg.type);

  /// Checks if message passes all of given [filters].
  static Filter every(List<Filter> filters) =>
      (Message msg) => filters.every((filter) => filter(msg));

  /// Checks if message passes any of given [filters].
  static Filter any(List<Filter> filters) =>
      (Message msg) => filters.any((filter) => filter(msg));
}
