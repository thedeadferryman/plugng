/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import 'package:uuid/uuid.dart';

import 'intercom.dart';
import 'messaging.dart';
import 'plugin_registry.dart';
import 'util.dart';

class _PluginInstance {
  static const _uuid = Uuid();

  final KnownPlugin plugin;
  final String uid;

  _PluginInstance(this.plugin, this.uid);

  _PluginInstance.unique(this.plugin) : uid = _uuid.v4();
}

class _PluginInstanceProxy {
  final ManagerIntercom intercom;
  final _PluginInstance instance;
  late int _lsid;

  _PluginInstanceProxy.create(KnownPlugin plugin)
      : instance = _PluginInstance.unique(plugin),
        intercom = ManagerIntercom(plugin.location);

  Future<void> initialize() async {
    if (intercom.isInitialized) return;

    _lsid = intercom.listener.addListener((data) {
      return _onInternal(data);
    });

    await intercom.initialize();
  }

  void detach() {
    if (intercom.isInitialized) {
      intercom.listener.removeListener(_lsid);
    }
  }

  void close() => intercom.close();

  bool _onInternal(data) => true;
}

class PluginCall extends Serializable {
  final String id;
  final String target;
  final String method;
  final dynamic args;
  final Duration? timeout;

  static PluginCall fromJson(Map map) {
    var proxy = TypedMapProxy(map);

    return PluginCall(
      proxy.getTyped('id'),
      proxy.getTyped('target'),
      proxy.getTyped('method'),
      timeout: _parseDuration(proxy.getTyped('timeout')),
      args: proxy['args'],
    );
  }

  static Duration? _parseDuration(int? mcs) =>
      (mcs == null) ? null : Duration(microseconds: mcs);

  PluginCall(this.id, this.target, this.method, {this.timeout, this.args});
  PluginCall.unique(this.target, this.method, {this.timeout, this.args})
      : id = Uuid().v4();

  @override
  Map toJson() => {
        'id': id,
        'target': target,
        'method': method,
        'args': (args is Serializable) ? args.toJson() : args,
        'timeout': timeout?.inMicroseconds,
      };
}

class CallRequest extends Serializable {
  final String id;
  final String method;
  final dynamic args;

  static CallRequest fromJson(Map map) {
    var proxy = TypedMapProxy(map);

    return CallRequest(
      proxy.getTyped('id'),
      proxy.getTyped('method'),
      proxy['args'],
    );
  }

  CallRequest(this.id, this.method, this.args);
  CallRequest.unique(this.method, this.args) : id = Uuid().v4();

  @override
  Map toJson() => {
        'id': id,
        'method': method,
        'args': (args is Serializable) ? args.toJson() : args,
      };
}

class CallResult extends Serializable {
  final String id;
  final dynamic result;
  final bool error;

  static CallResult fromJson(Map map) {
    var proxy = TypedMapProxy(map);

    return CallResult(proxy.getTyped('id'), proxy['result'],
        error: proxy.getTyped<bool?>('error') ?? false);
  }

  CallResult(this.id, this.result, {this.error = false});

  @override
  Map toJson() => {
        'id': id,
        'result': (result is Serializable) ? result.toJson() : result,
        'error': error
      };
}

class PluginNotFoundError extends Error {
  final PluginRequest request;

  PluginNotFoundError(this.request);
}

class PluginNotLoadedError extends Error {
  final String id;

  PluginNotLoadedError(this.id);
}

Filter _checkResultId(String id) => (Message result) =>
    CallResult.fromJson(assertTyped(result.content)).id == id;

class PluginManager {
  final PluginRegistry _registry;
  final Map<String, _PluginInstanceProxy> _loaded = {};
  final Map<String, List<String>> _depends = {};
  final Map<String, List<String>> _usedBy = {};

  PluginManager(this._registry) {
    _depends['master'] = [];
  }

  Future<String> loadPlugin(
    PluginRequest request, {
    String initiator = 'master',
  }) async {
    var plugin = _registry.findPlugin(request);

    if (plugin == null) {
      throw PluginNotFoundError(request);
    }

    var proxy = _PluginInstanceProxy.create(plugin);

    var lsid = proxy.intercom.listener.addPassthroughListener(
      (data) => _onPluginDataWrapper(proxy.instance.uid, data),
    );

    try {
      await proxy.initialize();
    } catch (e) {
      proxy.intercom.listener.removeListener(lsid);
      rethrow;
    }

    _registerInstance(proxy, initiator);

    proxy.intercom.sendSignal(Message(MessageType.START_SIGNAL));

    return proxy.instance.uid;
  }

  void unloadPlugin(String uid) {
    var proxy = _loaded[uid];

    if (proxy == null) return;

    proxy.intercom.sendSignal(Message(MessageType.STOP_SIGNAL));
    proxy.intercom.close();

    _depends[uid]?.forEach((dep) => unloadPlugin(dep));
    _depends.remove(uid);

    _usedBy.remove(uid);

    _loaded.remove(uid);
  }

  Future<dynamic> callPlugin(String uid, CallRequest request,
      {Duration? timeout}) {
    var target = _loaded[uid];

    if (target == null) {
      throw PluginNotLoadedError(uid);
    }

    var msg = Message(MessageType.CALL_REQUEST, request);

    return target.intercom.sendWaitWhere(
      msg,
      Filters.every([
        Filters.ofType([MessageType.PLUGIN_CALL_RESULT]),
        _checkResultId(request.id),
      ]),
      timeout: timeout,
    );
  }

  void _registerInstance(_PluginInstanceProxy proxy, String initiator) {
    _loaded[proxy.instance.uid] = proxy;
    _depends[proxy.instance.uid] = [];
    _usedBy[proxy.instance.uid] = [initiator];

    _depends[initiator]!.add(proxy.instance.uid);
  }

  void _onPluginDataWrapper(String uid, data) {
    if (!(data is Map)) return;

    try {
      var message = Message.fromJson(data);

      return _onPluginData(uid, message);
    } catch (e) {
      return;
    }
  }

  void _onPluginData(String uid, Message msg) async {
    switch (msg.type) {
      case MessageType.DEPENDENCY_REQUEST:
        var request = PluginRequest.fromJson(assertTyped(msg.content));
        return _dependencyRequested(uid, request);
      case MessageType.PLUGIN_DONE:
        return unloadPlugin(uid);
      case MessageType.PLUGIN_CALL:
        var call = PluginCall.fromJson(assertTyped(msg.content));
        return _pluginCalled(uid, call);
      default:
    }
  }

  void _dependencyRequested(String uid, PluginRequest request) async {
    var intercom = assertTyped<_PluginInstanceProxy>(_loaded[uid]).intercom;
    try {
      var depUid = await loadPlugin(request, initiator: uid);

      intercom.sendSignal(
        Message(
          MessageType.DEPENDENCY,
          Dependency(
            request.depName,
            depUid,
          ),
        ),
      );
    } on PluginNotFoundError {
      intercom
          .sendSignal(Message(MessageType.DEPENDENCY_MISSING, request.depName));
    }
  }

  void _pluginCalled(String uid, PluginCall call) async {
    var caller = _loaded[uid] ?? (throw Error());
    try {
      var result = await callPlugin(
        call.target,
        CallRequest(call.id, call.method, call.args),
      );
      return caller.intercom
          .sendSignal(Message(MessageType.CALL_RESPONSE, result.content));
    } on PluginNotLoadedError {
      caller.intercom.sendSignal(Message(MessageType.CALL_NO_PLUGIN, call.id));
    }
  }
}
