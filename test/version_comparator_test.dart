/*
 * Copyright 2021 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import 'package:plugng/plugng_manager.dart';
import 'package:test/test.dart';
import 'package:version/version.dart';

void main() {
  group('VersionCondition', () {
    group('.any', () {
      test('serialized correctly', () {
        expect(VersionCondition.any.toJson(), equals('*'));
        expect(VersionCondition.any.toString(), equals('*'));
      });

      test('parsed correctly', () {
        expect(VersionCondition.parse('*'), equals(VersionCondition.any));
      });

      test('matches any valid version', () {
        expect(VersionCondition.any.satisfies(Version.parse('1.0.0')), isTrue);
        expect(VersionCondition.any.satisfies(Version.parse('0.0.2')), isTrue);
        expect(VersionCondition.any.satisfies(Version.parse('5.3.1')), isTrue);
        expect(
            VersionCondition.any.satisfies(Version.parse('120.79.1')), isTrue);
      });
    });

    test('serialized/parsed correctly', () {
      var vc = VersionCondition.parse('>=1.2.3,<=4.2.0,==3.3.1');

      expect(vc.toString(), equals('>=1.2.3,<=4.2.0,==3.3.1'));
    });

    test('parsed correctly with whitespaces', () {
      var vc = VersionCondition.parse('>=1.2.3  ,  <=4.2.0  ,  ==3.3.1');

      expect(vc.toString(), equals('>=1.2.3,<=4.2.0,==3.3.1'));
    });

    test('parses version w/o conditional as equality', () {
      var version = Version.parse('1.2.4');

      var vc = VersionCondition.parse(version.toString());

      expect(vc.toString(), equals('==$version'));
    });

    test('matches equality', () {
      var version = Version.parse('4.2.0');

      expect(VersionCondition.parse('==$version').satisfies(version), isTrue);
    });

    test('matches contradictory equalities to neither', () {
      var vc = VersionCondition.parse('==4.2.0,==2.7.1');

      expect(vc.satisfies(Version.parse('4.2.0')), isFalse);
      expect(vc.satisfies(Version.parse('2.7.1')), isFalse);
    });

    test("matches 'strict greater'", () {
      var vc = VersionCondition.parse('>1.2.1');

      expect(vc.satisfies(Version.parse('1.2.5')), isTrue);
      expect(vc.satisfies(Version.parse('1.3.0')), isTrue);
      expect(vc.satisfies(Version.parse('2.0.7')), isTrue);
      expect(vc.satisfies(Version.parse('1.2.1')), isFalse);
      expect(vc.satisfies(Version.parse('1.1.9')), isFalse);
      expect(vc.satisfies(Version.parse('0.6.1')), isFalse);
      expect(vc.satisfies(Version.parse('0.0.27')), isFalse);
    });

    test("matches 'strict less'", () {
      var vc = VersionCondition.parse('<1.2.1');

      expect(vc.satisfies(Version.parse('1.1.9')), isTrue);
      expect(vc.satisfies(Version.parse('0.6.1')), isTrue);
      expect(vc.satisfies(Version.parse('0.0.27')), isTrue);
      expect(vc.satisfies(Version.parse('1.2.1')), isFalse);
      expect(vc.satisfies(Version.parse('1.2.5')), isFalse);
      expect(vc.satisfies(Version.parse('1.3.0')), isFalse);
      expect(vc.satisfies(Version.parse('2.0.7')), isFalse);
    });

    test("matches 'greater-or-equal'", () {
      var vc = VersionCondition.parse('>=1.2.1');

      expect(vc.satisfies(Version.parse('1.2.1')), isTrue);
      expect(vc.satisfies(Version.parse('1.2.5')), isTrue);
      expect(vc.satisfies(Version.parse('1.3.0')), isTrue);
      expect(vc.satisfies(Version.parse('2.0.7')), isTrue);
      expect(vc.satisfies(Version.parse('1.1.9')), isFalse);
      expect(vc.satisfies(Version.parse('0.6.1')), isFalse);
      expect(vc.satisfies(Version.parse('0.0.27')), isFalse);
    });

    test("matches 'less-or-equal'", () {
      var vc = VersionCondition.parse('<=1.2.1');

      expect(vc.satisfies(Version.parse('1.2.1')), isTrue);
      expect(vc.satisfies(Version.parse('1.1.9')), isTrue);
      expect(vc.satisfies(Version.parse('0.6.1')), isTrue);
      expect(vc.satisfies(Version.parse('0.0.27')), isTrue);
      expect(vc.satisfies(Version.parse('1.2.5')), isFalse);
      expect(vc.satisfies(Version.parse('1.3.0')), isFalse);
      expect(vc.satisfies(Version.parse('2.0.7')), isFalse);
    });

    test('throws FormatException when parsing invalid version', () {
      expect(() => VersionCondition.parse('>2#.r98.e'),
          throwsA(isA<FormatException>()));
    });

    test('throws FormatException when parsing an invalid conditional', () {
      expect(() => VersionCondition.parse('&1.2.3'),
          throwsA(isA<FormatException>()));
    });
  });
}
